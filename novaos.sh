#!/usr/bin/env bash

#      _ _   _
#     | | \ | |
#  _  | |  \| |  JamesNova (JN)
# | |_| | |\  |  gitlab.com/JamesNova
#  \___/|_| \_|
#

OUTPUT=/tmp/output.txt
PKGLIST=pkglist.txt
AURPKGS=aurpkgs.txt

if [ "$(id -u)" = 0 ]; then
    echo "##################################################################"
    echo "This script MUST NOT be run as root user since it makes changes"
    echo "to the \$HOME directory of the \$USER executing this script."
    echo "The \$HOME directory of the root user is, of course, '/root'."
    echo "We don't want to mess around in there. So run this script as a"
    echo "normal user. You will be asked for a sudo password when necessary."
    echo "##################################################################"
    exit 1
fi

error() { \
    clear; printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;
}

echo "################################################################"
echo "## Syncing the repos and installing 'dialog' if not installed ##"
echo "################################################################"
sudo pacman --noconfirm --needed -Syu dialog || error "Error syncing the repos."

welcome() { \
    dialog --title "Installing NovaOS!" --msgbox "This is a script that will install NovaOS. We will add NovaOS repos to Pacman and install the a tiling window manager, a panel, the Alacritty terminal, the Fish shell and many other essential programs needed to make my dotfiles work correctly.\\n\\n-JN (James Nova)" 16 60

    dialog --title "Stay near your computer!" --yes-label "Continue" --no-label "Exit" --yesno "This script is not allowed to be run as root, but you will be asked to enter your sudo password at various points during this installation. This is to give PACMAN the necessary permissions to install the software.  So stay near the computer." 10 60
}

welcome || error "User choose to exit."

dmsel() { \
    dialog --title "Display Manager" --menu "Select your display manager or log-in screen:" 16 60 4 Startx "Use xorg xinit to start your WM" Lightdm "A very good looking DM with the Aether theme" Ly "A very simple and minimalist DM" Other "Install it yourself later" 2>$OUTPUT
    DM=$(cat $OUTPUT)
}

dmsel || error "User choose to exit."

wmsel() { \
    dialog --title "Window Manager" --menu "Select your window manager:" 16 60 3 XMonad "My main WM is the most customized and has a lot of features written in haskell" Dwm "A very simple WM made by Suckless" Other "Install it yourself later" 2>$OUTPUT
    WM=$(cat $OUTPUT)
}

wmsel || error "User choose to exit."

edsel() { \
   dialog --title "Editor" --menu "Select your editor:" 16 60 4 Emacs "Doom Emacs" Neovim "Neovim" All "Install all of them" Other "Install it yourself later" 2>$OUTPUT
   ED=$(cat $OUTPUT)
}

edsel || error "User choose to exit."

bwsel() { \
    dialog --title "Browser" --menu "Select your Browser:" 16 60 5 Firefox "A custom css firefox" Brave "A secure focused browser" Qutebrowser "A fast and simple browser" All "Install all of them" Other "Install it yourself later" 2>$OUTPUT
    BW=$(cat $OUTPUT)
}

bwsel || error "User choose to exit."

tmsel() { \
    dialog --title "Terminal" --menu "Select your terminal emulator:" 16 60 4 Alacritty "A fast terminal" St "A simple terminal" All "Install all of them" Other "Insall it yourself later" 2>$OUTPUT
    TM=$(cat $OUTPUT)
}

tmsel || error "User choose to exit."

shsel() { \
    dialog --title "Shell" --menu "Choose your default shell:\n\nNote: bash is installed by default so if you choose 'Other' you will have bash but it will not have my custom .bashrc" 16 60 4 Bash "The basic shell" Zsh "A powerful customizable shell" Fish "A friendly shell for begginers" Other "Install it yourself later" 2>$OUTPUT
    SH=$(cat $OUTPUT)
}

shsel || error "User choose to exit."

lastchance() { \
    dialog --title "Installing NovaOS!" --msgbox "WARNING! The NovaOS installation script is currently in public beta testing. There are almost certainly errors in it; therefore, it is strongly recommended that you not install this on production machines. It is recommended that you try this out in either a virtual machine or on a test machine." 16 60

    dialog --title "Are You Sure You Want To Do This?" --yes-label "Begin Installation" --no-label "Exit" --yesno "Shall we begin installing NovaOS?" 8 60 || { clear; exit 1; }
}

lastchance || error "User choose to exit."

case $DM in
  Startx)
    echo "novaos-xinit" >> $PKGLIST
  ;;
  Lightdm)
    echo "lightdm" >> $PKGLIST
    echo "lightdm-webkit2-greeter" >> $PKGLIST
    echo "lightdm-webkit-theme-aether" >> $AURPKGS
  ;;
  Ly)
    echo "ly" >> $AURPKGS
  ;;
  *)
  ;;
esac
case $WM in
  XMonad)
    echo "xmonad" >> $PKGLIST
    echo "novaos-xmonad" >> $PKGLIST
    echo "xmonad-contrib" >> $PKGLIST
    echo "xmobar" >> $PKGLIST
    echo "novaos-xmobar" >> $PKGLIST
  ;;
  Dwm)
    echo "dwm-jn" >> $PKGLIST
  ;;
  *)
  ;;
esac
case $ED in
  Emacs)
    echo "emacs" >> $PKGLIST
  ;;
  Neovim)
    echo "neovim" >> $PKGLIST
    echo "novaos-neovim" >> $PKGLIST
  ;;
  ALL)
    echo "emacs" >> $PKGLIST
    echo "neovim" >> $PKGLIST
    echo "novaos-neovim" >> $PKGLIST
  ;;
  *)
  ;;
esac
case $BW in
  Firefox)
    echo "firefox" >> $PKGLIST
  ;;
  Brave)
    echo "brave" >> $PKGLIST
  ;;
  Qutebrowser)
    echo "qutebrowser" >> $PKGLIST
  ;;
  All)
    echo "firefox" >> $PKGLIST
    echo "brave" >> $PKGLIST
    echo "qutebrowser" >> $PKGLIST
  ;;
  *)
  ;;
esac
case $TM in
  Alacritty)
    echo "alacritty" >> $PKGLIST
    echo "novaos-alacritty" >> $PKGLIST
  ;;
  St)
    echo "st-jn" >> $PKGLIST
  ;;
  All)
    echo "alacritty" >> $PKGLIST
    echo "novaos-alacritty" >> $PKGLIST
    echo "st-jn" >> $PKGLIST
  ;;
  *)
  ;;
esac
case $SH in
  Bash)
    echo "novaos-bash" >> $PKGLIST
  ;;
  Zsh)
    echo "zsh" >> $PKGLIST
    echo "novaos-zsh" >> $PKGLIST
  ;;
  Fish)
    echo "fish" >> $PKGLIST
    echo "novaos-fish" >> $PKGLIST
  ;;
  *)
  ;;
esac

addrepo() { \
    echo "#########################################################"
    echo "## Adding the NovaOS core repository to /etc/pacman.conf ##"
    echo "#########################################################"
    grep -qxF "[core-repo-novaos]" /etc/pacman.conf ||
        (echo "[core-repo-novaos]"; echo "SigLevel = Optional TrustAll"; \
        echo "Server = https://gitlab.com/JamesNova/core-repo-novaos/-/raw/master/x86_64") | sudo tee -a /etc/pacman.conf
    grep -qxF "[multilib]" /etc/pacman.conf ||
        (echo "[multilib]"; echo "Include = /etc/pacman.d/mirrorlist") | sudo tee -a /etc/pacman.conf
}

addrepo || error "Error adding NovaOS repo to /etc/pacman.conf."

# Add sudo no password rights
sudo sed -i 's/^# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers

#Add parallel downloading
sudo sed -i 's/^#ParallelDownloads = 5/ParallelDownloads = 5/' /etc/pacman.conf

sudo pacman -Syyu --noconfirm

# Let's install each package listed in the pkglist.txt file.
sudo pacman --needed --noconfirm -Sy - < $PKGLIST
# Installing packages listed in aurpkgs.txt file from the AUR.
yay -S --noconfirm - < $AURPKGS

echo "#####################################################################"
echo "## Copying NovaOS configuration files from /etc/novaos into \$HOME ##"
echo "#####################################################################"
[ ! -d /etc/novaos ] && sudo mkdir /etc/novaos
[ -d /etc/novaos ] && mkdir ~/novaos-backup-$(date +%Y.%m.%d-%H%M) && cp -Rf /etc/novaos ~/novaos-backup-$(date +%Y.%m.%d-%H%M)
[ ! -d ~/.config ] && mkdir ~/.config
[ -d ~/.config ] && mkdir ~/.config-backup-$(date +%Y.%m.%d-%H%M) && cp -Rf ~/.config ~/.config-backup-$(date +%Y.%m.%d-%H%M)
cd /etc/novaos && cp -Rf . ~ && cd -

# Change all scripts in .local/bin to be executable.
find $HOME/.local/bin -type f -print0 | xargs -0 chmod 775

[ ! -d $HOME/.config/dmscripts ] && mkdir $HOME/.config/dmscripts
cp /etc/dmscripts/config $HOME/.config/dmscripts/config
sed -i 's/DMBROWSER=\"brave\"/DMBROWSER=\"qutebrowser\"/g' $HOME/.config/dmscripts/config
sed -i 's/DMTERM=\"st -e\"/DMTERM=\"alacritty -e\"/g' $HOME/.config/dmscripts/config
sed -i 's/setbg_dir=\"${HOME}\/Pictures\/Wallpapers\"/setbg_dir=\"\/usr\/share\/backgrounds\/dtos-backgrounds\"/g' $HOME/.config/dmscripts/config

if [ $DM = "Lightdm" ]; then
  sudo systemctl enable lightdm
fi
if [ $DM = "Ly" ]; then
  sudo systemctl enable ly
fi

if [ $WM = "XMonad" ]; then
  [ ! -d /etc/pacman.d/hooks ] && sudo mkdir /etc/pacman.d/hooks
  sudo cp /etc/dtos/.xmonad/pacman-hooks/recompile-xmonad.hook /etc/pacman.d/hooks/
  sudo cp /etc/dtos/.xmonad/pacman-hooks/recompile-xmonadh.hook /etc/pacman.d/hooks/
  xmonad --recompile
  ghc -dynamic "$HOME"/.xmonad/xmonadctl.hs
fi

if [ $ED = "Emacs" ] || [ $ED = "All" ]; then
  git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d
  ~/.emacs.d/bin/doom install
fi
if [ $ED = "Neovim" ] || [ $ED = "All" ]; then
  sudo npm i -g pyright bash-language-server neovim
fi

case $SH in
  Bash)
    sudo chsh $USER -s /bin/bash
    break
  ;;
  Zsh)
    sudo chsh $USER -s /bin/zsh
    break
  ;;
  Fish)
    sudo chsh $USER -s /bin/fish
    break
  ;;
  *)
    break
  ;;
esac

sudo sed -i 's/^%wheel ALL=(ALL) NOPASSWD: ALL/# %wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers

echo "################################"
echo "## NovaOS has been installed! ##"
echo "################################"

echo "Now reboot to boot into NovaOS"

while true; do
    read -p "Do you want to reboot to get your dtos? [Y/n] " yn
    case $yn in
        [Yy]* ) reboot;;
        [Nn]* ) break;;
        "" ) reboot;;
        * ) echo "Please answer yes or no.";;
    esac
done
